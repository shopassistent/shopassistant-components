/* global module */

module.exports = function (grunt) {
    'use strict';

    grunt.initConfig({
        // Metadata
        pkg: grunt.file.readJSON('package.json'),


        // -- Tasks
        clean: [
          'dist/**', 'build/**'
        ],
        ngtemplates:{
            components:{
                src: ['src/**/*.html',
                    // exclude static pages from user management component
                    '!**/choose_password.html',
                    '!**/password_reset_success.html'],
                dest: 'build/components.tpl.js',
                options: {
                    collapseWhitespace: true,
                    collapseBooleanAttributes: true
                }
            }
        }, // ngTemplates
        concat: {
            components: {
                src: ['src/**/*.js'],
                dest: 'build/components.js'
            }
        },// concat
        uglify: {
            minified: {
                options: {
                    banner: '/* <%= pkg.name %> - v<%= pkg.version %> - <%= grunt.template.today("yyyy-mm-dd") %>' +
                    '* by <%= pkg.author %> (http://shopper.com)' +
                    '*/'
                },
                // copy static pages from user management component
                files: {
                    'dist/components.min.js': ['build/components.js'],
                    'dist/components.tpl.min.js': ['build/components.tpl.js']
                }
            } // minified
        }, // uglify
        copy: {
            components: {
                files:[{
                    expand:true,
                    flatten: true,
                    src: ['build/components.js', 'build/components.tpl.js'],
                    dest: 'dist/'
                }]
            },
            userManagement:{
                files:[{
                    expand:true,
                    flatten: true,
                    src: ['src/user-management/choose_password.html',
                        'src/user-management/password_reset_success.html'],
                    dest: 'dist/'
                }]
            }
        }, //copy
    }),

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-clean');
    grunt.loadNpmTasks('grunt-angular-templates');


    grunt.event.on('watch', function(action, filepath, target) {
        grunt.log.writeln(target + ': ' + filepath + ' has ' + action);
    });


    // -- Tasks

    grunt.registerTask('default', ['clean', 'ngtemplates', 'concat', 'uglify', 'copy']);
};
