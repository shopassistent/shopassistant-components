
(function(angular) {
    'use strict';

    var controllerId = 'UserManagementController';
    angular.module('parse.user-management').controller(controllerId, ['$sce', '$state', controller]);

    function controller($sce, $state) {
        var vm = this;
        var params = [];
        if($state.params.token){
            params.push('token=' + $state.params.token);
        }

        if($state.params.username){
            params.push('username=' + encodeURIComponent($state.params.username));
        }

        var frameUrl = 'https://www.parse.com' + decodeURIComponent($state.params.link);
        if(params.length > 0){
            frameUrl += '?' + params.join('&');
        }
        console.log(frameUrl);
        vm.frameUrl = $sce.trustAsResourceUrl(frameUrl);
    }

})(angular);