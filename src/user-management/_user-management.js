(function (angular) {
    'use strict';

    angular
        .module('parse.user-management', ['ui.router'])
        .config(['$stateProvider', 'user-management.state', config]);

    function config($stateProvider, state) {

        console.log(state);
        $stateProvider.state(state,
            {
                url: '/user-management?link&token&username',
                template: '<iframe class="user-management" src="{{vm.frameUrl}}"></iframe>',
                controller: 'UserManagementController as vm'
            });
    }

})(angular);
