angular.module('components').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('src/user-management/reset-password-modal.html',
    "<form name=\"resetPassword\" data-ng-submit=\"vm.submit(resetPassword)\" novalidate>\n" +
    "    <iframe id=\"reset_password_iframe\" src=\"{{vm.frameUrl}}\" onload=\"passwordResetSuccess()\">\n" +
    "    </iframe>\n" +
    "</form>\n" +
    "\n" +
    "\n"
  );

}]);
