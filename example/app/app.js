(function (angular) {
    'use strict';

    angular
        .module('app', ['ui.router','parse.user-management'])
        .config(['$urlRouterProvider', '$stateProvider', config]);

    function config($urlRouterProvider, $stateProvider) {

        $urlRouterProvider.otherwise('/');

        $stateProvider.state('template',
            {
                abstract: true,
                templateUrl: 'app/template.html'
            });
        $stateProvider.state('template.home',
            {
                url: '/',
                templateUrl: 'app/home.html'
            });
    }

    angular
        .module('parse.user-management')
        .constant('user-management.state', 'template.user-management')

})(angular);
